import libvirt

from vm_control import switch_vms


def connect(url='qemu:///system'):
    """Opens and returns the libvirt connection."""
    return libvirt.open(url)

def get_running_vms(conn):
    """Returns a list of the running VM objects."""
    ids = conn.listDomainsID()
    return [conn.lookupByID(id) for id in ids]

def get_all_defined_vms(conn):
    """Returns a list of all the defined VM objects."""
    return conn.listAllDomains()

def get_non_running_vms(conn):
    """Returns a list of VM objects for all defined VMs that are not currently running."""
    defined_vms = get_all_defined_vms(conn)
    return [vm for vm in defined_vms if vm.state()[0] != libvirt.VIR_DOMAIN_RUNNING]

def filter_vms_by_name(vm_list, filter_str):
    """Takes a list of VMs, and returns a filtered list by name."""
    return [vm for vm in vm_list if filter_str in vm.name()]

def is_vm_running_by_name(conn, vm_name):
    """Returns id a VM is running, by it's name."""
    return conn.lookupByName(vm_name).state()[0] == libvirt.VIR_DOMAIN_RUNNING

def vm_names(vm_list):
    """Takes a list of VM objects and returns a list of the VM names."""
    return [vm.name() for vm in vm_list]

def guess_running_vm(conn, filter_str):
    """Guesses which running VM to swtich by the name filter."""
    running_vms = get_running_vms(conn)
    filtered_running_vms = filter_vms_by_name(running_vms, filter_str)
    if len(filtered_running_vms) == 0:
        print(f"No running VMs match named filter'{filter_str}'")
        return None
    elif len(filtered_running_vms) == 1:
        guess_vm = filtered_running_vms[0]
        print(f"Guessing the filtered running VM is {guess_vm.name()}.")
        return guess_vm
    else:
        print(f"More than one running VM matched with filter '{filter_str}'")
        return user_select_vm(running_vms, prompt_msg="Please select which filtered running VM to switch from: ")

def list_vms(vm_list):
    """Prints out VMs in a numbered list for selecting."""
    ind = 1
    for vm in vm_list:
        print(f"{ind}. {vm.name()}")
        ind += 1

def user_select_vm(vm_list, prompt_msg="VM to switch to [0 to exit]: "):
    """Prompts user to select VM by index and then returns the selected VM obj (or None)."""
    if vm_list:
        list_vms(vm_list)
        max_ind = len(vm_list)
        ind = None
        while not ind:
            selection = input(prompt_msg)
            try:
                ind = int(selection)
                if ind > max_ind:
                    print(f"Please select a number between 1 and {max_ind}, or 0 to exit")
                    ind = None
                elif ind == 0:
                    print("'0' Selected, so exiting.")
                    ind = True ## To exit loop
                    selection = None
                elif ind < 0:
                    print("Please enter a value >= 0 please.")
                    ind = None
                else:
                    print(f"'{ind}'' selected, so VM: {vm_list[ind-1].name()}")
                    selection = vm_list[ind-1]
            except ValueError:
                ind = None
                print(f"Invalid selection. Please select a number between 1 and {max_ind}, or 0 to exit")
        if ind == True:
            return selection
        else:
            return vm_list[ind-1]
    else:
        print("Error: Supplied VM list empty.")

# Dirty (functionally)
def prompt_switch_vm(conn, filter_str):
    """Prints the potential vfio VMs to switch to, and prompts the user for
       one. Returns the selected VM obj or None."""
    potential_vms = filter_vms_by_name(get_non_running_vms(conn), filter_str)


def main():
    filter_str = 'vfio'
    conn = connect()
    guessed_vm = guess_running_vm(conn, filter_str)
    potential_vms = filter_vms_by_name(get_non_running_vms(conn), filter_str)
    if potential_vms:
        print("\nPlease select one of the following VMs to switch to:")
        selected_vm = user_select_vm(potential_vms)
        if selected_vm:
            print(f"\nSwitching from {guessed_vm.name()} to {selected_vm.name()}.")
            switch_vms(guessed_vm, selected_vm)
    else:
        print(f"No potential VMs found using the filter: {filter_str}")


if __name__ == "__main__":
    main()