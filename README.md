# vfio-pokebelt

A cli app to easily shutdown and switch from one VM with vfio passhtrough, to another.

## Background/Why the name...

Over the years, I have used names from the origonal 151 pokemon as the naming convention for my computers. This works rather nicely when using static ip addresses, as the last number value can be set to match that pokemon's number. For example, `192.168.1.*7*` for Charzard, or `192.168.1.*25*` for Pikachu.

Recently, I made some upgrades to my desktop that have allowed me to easily run VMs with VFIO passthrough to pass a gpu, my usb controller, and sometimes a ssd to my VMs. Currently, I am thinking about switching to have the host virtualize all of my desktops on the workstation, and I can switch around as I please. However, when I want to switch from one VM to another, I have to shut the first one down, *then* boot the second one so that the passed through resources can safely be transferred (the two VMs can't be run a the same time if they use the same hardware). I currently do this from the hosts's GUI, but I want to start doing it headless, so I can keep my second gpu available to run a second VM if I desire.

I started testing it using some roughly paried `virsh` one-liners, which seemed to work well enough. I realized I could wrap some basic code/automation around the process to make it much easier for me to do when it becomes an everyday thing. Thus, the idea for this project was born.

So... I decided to name it `vfio-pokebelt`. Because my computers (and soon to be VMs too) are poke'mon named, the idea of switching from one active one to the other reminded me of switching poke'mon during a battle in the old GameBoy games. The active pokemon would be switched with the others in your bag and/or ... pokemon belt (a belt that literally just had pokeballs attached to it). So, for me at least... it's fitting.


## Goals
I have some rough ideas about what I want this to be able to do at a bare min, and then some fancier stuff if I ever get to it.

**Basic Goals - Script**
- [x] Stops one VM and starts second when done
- [x] Uses a polling loop to hold command to start second VM until *after* it has verified the first is shutdown
- [x] Can grep for `-vfio` named VMs for easy selection (or other identifier)
- [x] Can grep for current running `-vfio` vm to default the switch with for even easier use

**CLI Tool**
- [ ] Convert to a python package, with executable
- [ ] Add command line args to cli
- [ ] Add config file to set some of these option values in
