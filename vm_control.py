# Note: some of these functions were inspired by code found here:
# https://github.com/avollmerhaus/virsh-start-stop/blob/master/src/virsh_start_stop/virsh_start_stop.py
#

import libvirt
import time


def switch_vms(running_vm, switch_vm, wait_time=5, timeout=180):
    """Shutsdown the running VM, waits a little, then starts the second."""
    print(f"Going to attempt to stop {running_vm.name()}")
    stop_vm(running_vm)
    print(f"Waiting for {wait_time} seconds...")
    time.sleep(wait_time)
    print(f"Going to try to start {switch_vm.name()}")
    start_vm(switch_vm)

def stop_vm(stop_vm, timeout=180):
    """Stops the VM and polls until the status is complete. Timeouts eventually."""
    elapsed_seconds = 0

    while not stop_vm.state()[0] == libvirt.VIR_DOMAIN_SHUTOFF:
        # issuing ACPI shutdown a few times can sometimes convince windows guests
        # to take the request seriously instead of displaying "really shutdown?"
        # on the virtual machines console.
        try:
            stop_vm.shutdownFlags(libvirt.VIR_DOMAIN_SHUTDOWN_GUEST_AGENT | libvirt.VIR_DOMAIN_SHUTDOWN_ACPI_POWER_BTN)
        except libvirt.libvirtError:
            # we have a race condition between checking for VIR_DOMAIN_SHUTOFF
            # and issuing the shutdown request.
            # unfortunately, "libvirtError" is all we get in that case.
            time.sleep(1)
            if stop_vm.state()[0] == libvirt.VIR_DOMAIN_SHUTOFF:
                # if the domain is in SHUTOFF now, we assume all went well
                pass
            else:
                # if not, re-raise
                raise
        time.sleep(1)
        elapsed_seconds += 1
        if elapsed_seconds == timeout:
            # graceful as in "send sigterm to qemu-kvm", this is still akin to yanking the virtual power cord
            stop_vm.destroyFlags(libvirt.VIR_DOMAIN_DESTROY_GRACEFUL)
    print(f"{stop_vm.name()}: shutdown took {elapsed_seconds} seconds.")


def start_vm(start_vm):
    """Starts up a VM."""
    if not start_vm.state()[0] == libvirt.VIR_DOMAIN_RUNNING:
        # there is a potential race condition here as well should some other process start the machine in between
        # let's see if that is really a problem in production before wrapping in try / except
        start_vm.create()
        print(f"{start_vm.name()} started. It might take a few more seconds to boot up.")
    else:
        print(f"{start_vm.name()}: libvirt reported state VIR_DOMAIN_RUNNING, assuming it\'s true")